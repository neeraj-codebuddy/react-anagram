import { useState } from "react";
import "./App.css";

function App() {
  const [value, setValue] = useState("");
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [anagrams, setAnagrams] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const findAnagram = (text) => {
    const arr = text.split("\n");
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].length === value.length) {
        if (
          arr[i].split("").sort().join("") === value.split("").sort().join("")
        ) {
          setAnagrams([...anagrams, arr[i]]);
        }
      }
    }
    setIsLoading(false);
    setEndTime(new Date());
  };

  const handleClick = () => {
    if (value.trim().length > 0) {
      setAnagrams([]);
      setIsLoading(true);
      setStartTime(new Date());
      fetch("/words.txt")
        .then((r) => r.text())
        .then((text) => {
          findAnagram(text);
        });
    }
  };

  return (
    <div className="App">
      <div>
        <h1>Anagram Finder</h1>
        <input
          value={value}
          onChange={(e) => setValue(e.target.value.toLowerCase())}
          disabled={isLoading}
        />
      </div>
      <button onClick={handleClick} disabled={isLoading} className="find-btn">
        <span>Find anagram</span>
      </button>
      {isLoading && <p>Finding anagrams</p>}
      {anagrams.length > 0 && (
        <div>
          <h2>Anagrams</h2>
          {anagrams.map((anagram, index) => (
            <li key={index}>{anagram}</li>
          ))}
        </div>
      )}
      {anagrams.length > 0 && (
        <div>Time taken: {(endTime - startTime) / 1000} seconds</div>
      )}
    </div>
  );
}

export default App;
